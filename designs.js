
//Shrouk Ali
// Select color input
// Select size input

// When size is submitted by the user, call makeGrid()

function makeGrid() {
      const myNode = document.getElementById("myTable");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
     10, width= 10;
    var height = document.getElementById("inputHeight").value;
    var width = document.getElementById("inputWidth").value;

    for(var i=0;i<height;i++){
          var tr = document.createElement("TR");
          for( var j=0;j<width;j++){
              var x = document.createElement("TD");
              var t = document.createTextNode("");
              x.appendChild(t);
              tr.appendChild(x)    
          }

          document.getElementById("myTable").appendChild(tr);
    }


    //using JQuery
/*   var cell = $('td')
    cell.click(function(){
        $(this).css("background-color", "yellow");
    });
    
*/  
    
    var cell = document.getElementsByTagName("TD");
    for(i=0;i<cell.length;i++) { 
        cell[i].addEventListener("click", function(){ 
            var color = document.getElementById("color-picker").value;
            this.style.backgroundColor = color;
        });
    }
    

}
    
    




var submitInput = document.getElementById("submitInput");

submitInput.addEventListener("click", function(event){
  event.preventDefault();
  makeGrid();
});


// using query
/*var submitInput = $('input[type="submit"]')

submitInput.click(function(e) {
  e.preventDefault();
  makeGrid();
}); */